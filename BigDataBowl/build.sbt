name := "BigDataBowl"

version := "0.1"

scalaVersion := "2.11.11"

val sparkVersion = "2.3.0"

resolvers += "cloudera" at "https://repository.cloudera.com/artifactory/cloudera-repos"
resolvers += "maven" at "https://mvnrepository.com/artifact"

libraryDependencies += "org.apache.spark" %% "spark-core" % sparkVersion

libraryDependencies += "org.apache.spark" %% "spark-sql" % sparkVersion


        